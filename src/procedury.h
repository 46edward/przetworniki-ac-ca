// "procedury.h" 

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
float Reg_w(float uchyb)//reg. predkosci 
{
	//nastawy reg. PI napiecia Udc
	float const	 Ti_w	=	20;			
	float const	 Kp_w	=	0.01;
	float const	 MAX	=	5.0;
	static float sum,sumL,wyj;

	sumL=sum;	
	uchyb=uchyb*Kp_w;			//uchyb * Kp
	sum=sum+(uchyb*Ti_w)*Tp;	//calka
	wyj=(uchyb+sum);
	
	//ograniczenie zadawanej wartosci na MAX
	if( wyj>MAX)	{sum=sumL;wyj=MAX;}
	//ograniczenie zadawanej wartosci na -MAX
	if( wyj<-MAX)	{sum=sumL; wyj=-MAX;}
	return wyj;
}

void sincos(wektor &WSK)
{	register float	mod,mod1;	
	mod=sqrtf((WSK.a)*(WSK.a)+(WSK.b)*(WSK.b));
	if(mod==0) mod=0.00001;    //zabezpieczenie brzed dzieleniem przez zero
	mod1=1.0/mod;
	WSK.sink=((WSK.b)*mod1);
	WSK.cosk=((WSK.a)*mod1);
	WSK.mod=mod;	
} 

inline void	uvw2ab(wektor &WSK) 
	{
	WSK.a=d2n3*(WSK.u-0.5*WSK.v-0.5*WSK.w);
	WSK.b=jp3*(WSK.v-WSK.w);
	WSK.mod=sqrtf(WSK.a*WSK.a+WSK.b*WSK.b);
	}

inline void suma_sincosk(wektor &IN1, wektor &IN2, wektor &OUT)
	{
	OUT.sink=IN1.sink*IN2.cosk+IN1.cosk*IN2.sink; //A+B   
	OUT.cosk=IN1.cosk*IN2.cosk-IN1.sink*IN2.sink;
	}

inline void ab2dq(wektor &WSK, wektor &KAT)
	{
	(WSK.d)=(WSK.a)*KAT.cosk+(WSK.b)*KAT.sink;
	(WSK.q)=(WSK.b)*KAT.cosk-(WSK.a)*KAT.sink;	
	}
	
inline void ab2xy(wektor &WSK, wektor &KAT)
	{
	(WSK.x)=(WSK.a)*KAT.cosk+(WSK.b)*KAT.sink;
	(WSK.y)=(WSK.b)*KAT.cosk-(WSK.a)*KAT.sink;	
	}

inline void dq2ab(wektor &WSK, wektor &KAT)
	{
	WSK.a=WSK.d*KAT.cosk-WSK.q*KAT.sink;
	WSK.b=WSK.q*KAT.cosk+WSK.d*KAT.sink;
	}
	
inline void xy2ab(wektor &WSK, wektor &KAT)
	{
	WSK.a=WSK.x*KAT.cosk-WSK.y*KAT.sink;
	WSK.b=WSK.y*KAT.cosk+WSK.x*KAT.sink;
	}

inline void	ab2uvw(wektor &WSK)
{	register float p3n2b;
	p3n2b=p3n2*(WSK.b);//pierwiaster3/2 * beta
	(WSK.u)=(WSK.a);
	(WSK.v)=-0.5*(WSK.a)+(p3n2b); 
	(WSK.w)=-0.5*(WSK.a)-(p3n2b);
}

float sincosk2kat(wektor &WSK)		//sin i cos kata na kat w stopniach
{	float kat;
	if(WSK.sink==0) WSK.sink=0.0000001;
	kat=atan2(WSK.sink,WSK.cosk);//w radianach   *180/PI;
	WSK.kat=kat;
	return kat;	
}

//##############################################################################
inline int	sec(float sinK, float cosK)
{	int sec=1;//Sektor
	if (cosK>0.8660254)				sec=1;else
	if ((sinK>=0.5)&&(cosK>0.0))  	sec=2;else
	if ((sinK>=0.5)&&(cosK<0.0))  	sec=3;else
	if (cosK<=-0.8660254037)	  	sec=4;else
	if ((sinK<=-0.5)&&(cosK<0.0)) 	sec=5;else
	if ((sinK<=-0.5)&&(cosK>0.0)) 	sec=6;
	return sec;	
}



