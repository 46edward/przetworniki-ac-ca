//setup.h ADSP-21369 v.1
//PWM-FPGA
//amplituda nosnej (zakres 0-32767) - przerwania (SJ2 -CONV-FPGA)
       
#include 	<sru.h>
#include 	<21369.h>
#include 	<signal.h>
#include 	<sysreg.h>


//	#define			PWM_WIDTH	16000//(IRQ-5kHz)/\/\/\/\/\/
//	#define			PWM_WIDTH	8000//(IRQ-10kHz)/\/\/\/\/\/
	#define	    	PWM_WIDTH	4000//(IRQ-20kHz)/\/\/\/\/\/

#define 	f_ADC		(8e7/PWM_WIDTH)		//częstotliwość pracy przetwornikow [Hz]
#define 	Tp		 	(1.0/f_ADC)			//wpisz czest. kwarca [Hz]
#define		f_TIMER	10						//czestotliwosc przerwan Timera [Hz]
#define		f_TIMER0	10000					//czestotliwosc Timera0 (wyjscie -> DPI) [Hz]
#define 	f_OSC	 	(20e6)				//wpisz czest. kwarca [Hz]		
#define 	DSP_CLK 	(350e6)				//wpisz czest. DSP

unsigned int 	ADC_RATE	=	f_OSC/f_ADC;
unsigned int 	ADC_WSPW	=	(f_OSC/f_ADC-f_OSC/f_ADC*0.05);//wsp. wypełnienia sygnału 10%
unsigned int 	TMR_RATE	=	DSP_CLK/f_TIMER;
unsigned int 	TMR0_RATE=	DSP_CLK/f_TIMER0/2;
unsigned char 	znak[8];


void InitSRU()
{
	// Enable pull-up resistors on unused DAI pins
	* (volatile int *)DAI_PIN_PULLUP = 0xFFFF7;

	// Enable pull-up resistors on unused DPI pins
	* (volatile int *)DPI_PIN_PULLUP = 0x1FFC;

	//Generating Code for connecting : PCG_FSA to DAI_PIN4
	SRU (HIGH, PBEN04_I); 
	SRU (PCG_FSA_O, DAI_PB04_I); 

	//Generating Code for connecting : LOW to DPI_PIN14
	SRU (HIGH, DPI_PBEN14_I); 
	SRU (LOW, DPI_PB14_I); 

	//Generating Code for connecting : HIGH to DPI_PIN1
	SRU (HIGH, DPI_PBEN01_I); 
	SRU (HIGH, DPI_PB01_I); 

	//Generating Code for connecting : TIMER0 to DPI_PIN2
	SRU (HIGH, DPI_PBEN02_I); 
	SRU (TIMER0_O, DPI_PB02_I); 
}
//===========================================================


void PLL_setup()// 350MHz
{
	volatile int pmctlsetting;
    pmctlsetting= *pPMCTL;
    pmctlsetting &= ~(0xFF); //Clear

    pmctlsetting= SDCKR2|PLLM35|INDIV|DIVEN;
    *pPMCTL= pmctlsetting;
    pmctlsetting|= PLLBP;
    *pPMCTL= pmctlsetting;

    //Wait for around 4096 cycles for the pll to lock.
    for (int i=0; i<4096; i++) asm("nop;nop;");
          
    *pPMCTL ^= PLLBP;       //Clear Bypass Mode
    *pPMCTL |= (CLKOUTEN);  //and start clkout	
}


 //  --== FPGA ==-- --== FPGA ==-- --== FPGA ==-- --== FPGA ==-- --== FPGA ==--
void setupFPGA()
{
	set_flag(SET_FLAG3, SET_FLAG);

	//PWM 3-level
	*(p_CS_FPGA+0x00)=PWM_WIDTH; 			//pwm_max_g ACDC
	*(p_CS_FPGA+0x01)=(unsigned)-PWM_WIDTH; //pwm_min_g ACDC
	*(p_CS_FPGA+0x02)=PWM_WIDTH; 			//pwm_max_d ACDC
	*(p_CS_FPGA+0x03)=(unsigned)-PWM_WIDTH;	//pwm_max_d ACDC 	


	*(p_CS_FPGA+0x04)=5; 		//dead-time ACDC w us
	*(p_CS_FPGA+0x05)=1000; 	//Watchdog  ACDC (tryb PWM/D)
	*(p_CS_FPGA+0x06)=500; 		//ustawienia Watchdoga ACDC


	*(p_CS_FPGA+0x07)=PWM_WIDTH; 			//pwm_max_g DCAC
	*(p_CS_FPGA+0x08)=(unsigned)-PWM_WIDTH; //pwm_min_g DCAC
	*(p_CS_FPGA+0x09)=PWM_WIDTH; 			//pwm_max_d DCAC
	*(p_CS_FPGA+0x0A)=(unsigned)-PWM_WIDTH; //pwm_min_d DCAC
	
	*(p_CS_FPGA+0x0B)=5; 			//dead-time DCAC w us
	*(p_CS_FPGA+0x0C)=1000; 		//Watchdog  DCAC (tryb PWM/D)
	*(p_CS_FPGA+0x0D)=1000; 		//ustawienia Watchdoga DCAC (us)

	*(p_CS_FPGA+0x0E)=13; 		//ustawienia rozdzielczosci enkodera 0x0B-12bit.

	set_flag(SET_FLAG3, CLR_FLAG);

}


static void InitDPI()
{
	SRU2(UART0_TX_O,DPI_PB09_I); // UART transmit signal is connected to DPI pin 9
	SRU2(HIGH,DPI_PBEN09_I);
	SRU2(DPI_PB10_O,UART0_RX_I); // connect the pin buffer output signal to the UART0 receive
	SRU2(LOW,DPI_PB10_I);
	SRU2(LOW,DPI_PBEN10_I);      // disables DPI pin10 as input
}

static void UARTisr(int)
{
 	if (*pUART0IIR & UARTRBFI)
 		{
			//	if ( buf.in<BUF_SIZE) buf.buf_rx [buf.in++] = (*pUART0RBR & 0x1FF);
			//	timeout=0; 
			znak[0]=(char) (*pUART0RBR & 0x1FF);    
 		}
}


void UART_Send(unsigned char *xmit, int SIZE)
{
	int i;
	/* loop to transmit source array in core driven mode */   
  	for (i=0; i<SIZE; i++)
  	{
    	do { ;}// Wait for the UART transmitter to be ready
    	while ((*pUART0LSR & UARTTHRE) == 0);
       	*pUART0THR = xmit[i]; //Transmit a byte
   	}
  
/* poll to ensure UART has completed the transfer */
  	while ((*pUART0LSR & UARTTEMT) == 0)  {;} 
}


static void InitUART()
{
	*pUART0LCR = UARTDLAB;  
	*pUART0DLL = 0x39; 
   *pUART0DLH = 0x02;  
   *pUART0LCR = UARTWLS8;
   *pUART0RXCTL = UARTEN;       
   *pUART0TXCTL = UARTEN;
    
	*pPICR0 &= ~(0x1F); //Sets the UART0 receive interrupt to P0
	*pPICR0 |= (0x13); 
	*pUART0RXCTL = UARTEN;       //enables UART0 in receive mode
   *pUART0TXCTL = UARTEN;       //enables UART0 in core driven mode
   *pUART0IER   = UARTRBFIE;   // enables UART0 receive interrupt
	interrupt(SIG_P0,UARTisr); 
}


//--------------------------- setup()  ----------------------------
void setup()
{
	PLL_setup();
	InitDPI();
	InitUART();
	InitSRU();
	
	interrupt(SIG_TMZ,timer_isr); //przerwanie od timera (LOW PRIOYTET.)
	timer_set( TMR_RATE , 0);//   (DSP_CLK/Hz)


    * (volatile int *) TM0CTL = TIMODEPWM|  // PWM Out Mode 
                                PULSE|      // Positive edge is active 
                                PRDCNT;     // Count to end of period 

    * (volatile int *) TM0PRD = TMR0_RATE;		// Timer 0 period = 255 
    * (volatile int *) TM0W   = TMR0_RATE*0.5;	// Timer 0 Pulse width = 50% 
    * (volatile int *) TMSTAT = TIM0EN;			// enable timer 0 


	 *pAMICTL0=0;
	 *pAMICTL0=(AMIEN | BW32 | WS16 | HC2 | RHC2| IC2);// 
	  
		
	sysreg_write(sysreg_USTAT4,*pSYSCTL);
	sysreg_bit_set(sysreg_USTAT4,IRQ2EN);	//FLAG2 configure as IRQ2
	*pSYSCTL=sysreg_read(sysreg_USTAT4);
   sysreg_bit_set(sysreg_MODE2,IRQ2E);		//IRQ is edge sensitive interrupt
   interrupt(SIG_IRQ2, ADC_IRQ2);			//przerwanie od ADC (SJ4) 14bit MAX1324 		
	
	
 	
   *pSYSCTL	=*pSYSCTL&(~MSEN)&(~TMREXPEN);	//FLAG3 jako wyjscie (zeruj bit MSEN)
  	*pSYSCTL	=*pSYSCTL|IRQ2EN;						//Wlacz IRQ2
	*pPCG_CTLA0=0x40000000 | ADC_RATE;
	*pPCG_PW1=ADC_WSPW;
	
	 setupFPGA();
	 LCD_INIT(); 
	 timer_on(); 
}
//--------------------------- setup()  ----------------------------


