//metody.h

//#################################################################
float Normalizuj(float ADC, KalibPot &WSK)
{
	float Temp;
	int StrefaMartwa=0;
	if (ADC > WSK.max)								//sprwdz max
		{		
		 WSK.max=ADC; 								//zapisz max
		 WSK.offset=(WSK.max+WSK.min)*0.5;			//oblicz offset
		 WSK.norma=WSK.max-WSK.offset-StrefaMartwa;	//oblicz dzielnik
		}
	if (ADC < WSK.min) 								//sprawdz min
		{									
		 WSK.min=ADC	;							//zapisz min
		 WSK.offset=(WSK.max+WSK.min)*0.5;          //oblicz offset
		 WSK.norma=WSK.max-WSK.offset-StrefaMartwa;	//oblicz dzielnik
		}
	Temp = (ADC-WSK.offset)*(-1.0/WSK.norma);		//znormalizuj wynik w granicach od -1.0 do 1.0
	if (Temp<-1.0) 
		Temp = -1.0;
	if (Temp>1.0)  
		Temp = 1.0;
	return Temp;
	
}
//#################################################################
