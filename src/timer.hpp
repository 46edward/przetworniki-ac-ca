//Obsluga przerwania Timer-a, cz�stotliwos� w setup.h --> f_TIMER 

void timer_isr (int sig)
{
static int miga;
static int klawisze=0, klawisze_T =0;
static int sw_LED = 0;		 	        	//pami�� prze��cznik�w		

		miga++; if(miga>255) {miga=0;}
		SET_LED(miga);          			//ustawienie stanu diod LED na konsoli z potencjometrami

		klawisze = BUTTONS_RD();			//odczyt stanu przyciskow	
		BUZZER(false);						//wy��cz buzzer
	
		// tryb bistabilny	
		if(klawisze>klawisze_T)								//sprawdzenie wcisni�cia nowego przycisku
		{
		   int Delta = klawisze ^ klawisze_T;	   			//roznica w stanie klawiatury wzgl�dem stanu z t-1
			sw_LED = ((~Delta & sw_LED)|(Delta & ~sw_LED)); //odwrocenie bitu na wybranej pozycji
			BUZZER(true);									//w��cz buzzer
		}	
	
		klawisze_T=klawisze;						//zapam�tanie stanu klawiatury
		BUTTONS_LED(sw_LED);						//zapal przyciski
		LCD_BACKLIGHT(sw_LED & 0x01);				//P1 w��cza podswietlenie LCD	
}
