//Estymator strumieni 
	
	#define PSIn	0.5
	#define Mn		5.2		//moment znamionowy
	#define Un		50			//napiecie znamionowe
	#define p_Mn	1.0/Mn
	#define p		1.0		//liczba par biegunow
	#define nn		2820		//predkosc znamionowa
	#define Lm		0.16		 //indukcyjnosc glowna 0.26
	#define Lsr		0.00614	//rozproszenia 0.0044
	#define Rs		0.5	
	#define Rr		0.5
	
	#define Ls		(Lm+Lsr)
	#define Lr		(Lm+Lsr)
	#define Tr		Rr/Lr
	#define TrLm	Tr*Lm
	#define Lmr		Lm/Lr
	#define wsp_a	(Ls*Lr-Lm*Lm)/Lm
	#define wsp_b	(Ls*Lr-Lm*Lm)/Lr
	#define	cPSI	(1.0/Lm)		//stale przeliczenia strumiena do skali pradow.
	#define cM		((2.0*nn*0.1047)/(3.0*p*Un*p2))		//stale przeliczenia momentu do skali pradow.
	#define	cPSI1	(1.0/cPSI)				//jeden przez
	#define cM1		(1.0/cM)				//jeden przez
	#define cLsr1	(1.0/Lsr)				//jeden przez
	#define wrn		((3000.0/p-nn)*0.1047)	//znamionowa pulsacja wirnika
	
//definicja tablicy wektorow napiec przeksztaltnika 2-poziomowego
const dm float	Us_a[8]={0,-0.5,-0.5,-1.0,1.0,0.5 ,0.5,0};
const dm float	Us_b[8]={0,-p3n2,p3n2 ,0   ,0  ,-p3n2,p3n2,0};


//================================================================================
inline void	konf2Uab(volatile int &stan_kluczy, float UDC, wektor &WSK)
{
	volatile	float	Usa,Usb;
	Usa=Us_a[(stan_kluczy & 0x07)]*UDC*(2.0/3.0);
	Usb=Us_b[(stan_kluczy & 0x07)]*UDC*(2.0/3.0);
	WSK.a=Usa;
	WSK.b=Usb;
}//================================================================================
	
	
//================================================================================
void model_U(wektor &wPSI, wektor &wIs, wektor &wUs)
{
	volatile float	fmua,fmub,frdu,frqu,frau,frbu;
	volatile float	fa_dU,PSIaUU,fb_dU,PSIbUU;
	static 	volatile float ea,wya=0.0;
	static 	volatile float eb,wyb=0.0;	
	volatile static float	fsau=0.0,fsbu=0.0,fsauf=0.0,fsbuf=0.0;

	fsauf=fsauf+(wUs.a-Rs*wIs.a-wya)*Tp;//
	fsbuf=fsbuf+(wUs.b-Rs*wIs.b-wyb)*Tp;//
//------------ odejmij skladowa stala------------------
	ea=(fsauf-wya)*100.0;	// *czest. odciecia
	wya=wya+ea*Tp;
	eb=(fsbuf-wyb)*100.0;	// *czest. odciecia
	wyb=wyb+eb*Tp;

	wPSI.a=fsauf;
	wPSI.b=fsbuf;
}//================================================================================


//Estymator strumieni g��wnych z reg. w ab
void model_UI(wektor &wPSI, wektor &wIs, wektor &wUs, float kat)
{	
	float const	Treg=0.1;			
	float const	OTreg=(1.0/Treg);//10Hz
	float const	Kreg=100.0;

	static volatile float	wyk2a=0.0,wyk2b=0.0,frqir=0.001,frdir=0.001;
	static volatile float	fsau=0.0,fsbu=0.0,wyrega=0.0,wyregb=0.0;
	volatile float		isd,isq,ct,st,Fir,frai,frbi,fmarr,fmbrr,fmua,fmub;
	volatile float		uchyba,uchybb,frdu,frqu,wyregd,wyregq;
	volatile float		fsai,fsbi,fsau_ks,fsbu_ks,frau=0.001,frbu=0.001;
	
	//Model pradowy
	Fir=kat*p; ct=cosf(Fir); st=sinf(Fir);
	
	isd=wIs.a*ct+wIs.b*st;
	isq=wIs.b*ct-wIs.a*st;

	frdir=frdir+((TrLm*isd-Tr*frdir)*Tp);
	frqir=frqir+((TrLm*isq-Tr*frqir)*Tp);
	frai=frdir*ct-frqir*st;
	frbi=frqir*ct+frdir*st;

	fsai=wsp_b*wIs.a+Lmr*frai;//strumien stojana ze str. wirn.
	fsbi=wsp_b*wIs.b+Lmr*frbi;

	fmarr=(frai+Lsr*wIs.a)*Lmr;//strumien glowny ze str. wirn.
	fmbrr=(frbi+Lsr*wIs.b)*Lmr;
	//koniec modelu pradowego

	//Model napieciowy
	fsau=fsau+(wUs.a-Rs*wIs.a+wyrega)*Tp; 
	fsbu=fsbu+(wUs.b-Rs*wIs.b+wyregb)*Tp;

	frau=(Lr/Lm)*fsau-wsp_a*wIs.a; 	//strumien wirnika ze str. stojU.
	frbu=(Lr/Lm)*fsbu-wsp_a*wIs.b;

	fmua=(fsau-Lsr*wIs.a);			//strumien glowny ze str. stojU.
	fmub=(fsbu-Lsr*wIs.b);
	//koniec modelu napieciowego

	//Regulator modelu w ukladzie ab
	uchyba=(frai-frau)*Kreg;//*(P2+1.05);
	uchybb=(frbi-frbu)*Kreg;//*(P2+1.05);

	wyk2a=wyk2a+uchyba*OTreg*Tp;
	wyk2b=wyk2b+uchybb*OTreg*Tp;
	wyrega=uchyba+wyk2a;
	wyregb=uchybb+wyk2b;

	wPSI.a=fmua;
	wPSI.b=fmub;
}//================================================================================



//Estymator strumieni model I
void model_I(wektor &wPSI, wektor &wIs, float kat)
{
	volatile static float		isd,isq,ct,st,Fir,frai,frbi,fmarr,fmbrr,fmua,fmub;
	volatile static float		frdir,frqir,fsai,fsbi,frau=0.001,frbu=0.001;
	//Model pradowy
	Fir=kat*p; ct=cosf(Fir); st=sinf(Fir);
	
	isd=wIs.a*ct+wIs.b*st;     //pr�d stojana d
	isq=wIs.b*ct-wIs.a*st;		//pr�d stojana q

	frdir=frdir+((TrLm*isd-Tr*frdir)*Tp);
	frqir=frqir+((TrLm*isq-Tr*frqir)*Tp);
	frai=frdir*ct-frqir*st;
	frbi=frqir*ct+frdir*st;

	fsai=wsp_b*wIs.a+Lmr*frai;//strumien stojana ze str. wirn.
	fsbi=wsp_b*wIs.b+Lmr*frbi;

	fmarr=(frai+Lsr*wIs.a)*Lmr;//strumien glowny ze str. wirn.
	fmbrr=(frbi+Lsr*wIs.b)*Lmr;
	//koniec modelu pradowego=========================

	wPSI.a=fsai;
	wPSI.b=fsbi;
}
//================================================================================


//================================================================================
float predkosc(float polozenie1)              //parametrem do funkcji jest k�t w radianach 
{
    static float polozenie2;
    static float czas,delta,predkosc;
    static float e0,wy0;
    float x;
    czas = czas + Tp;			      //zliczamy czas
	if(czas >1e-3)					  //nie mniej ni� jedna milisekunda
		{
			delta = polozenie1 - polozenie2;			 //obliczamy r�nic� po�o�enia wzgl�dem tego co by�o "czas" temu
			polozenie2 = polozenie1;					 //zapami�tujemy nopolozenie1 po�o�enie
			if(fabs(delta)<PI){	predkosc=delta/(czas);}	 //predkosc bez filtra
			czas=0.0;					  				 //reset licznika czasu
		}	
	//filtr
	x=fabs(wy0);
	x=5.0+x*0.5;
	e0=(predkosc-wy0)*x;	// pasmo zmenne
	wy0=wy0+e0*Tp;
	return wy0;	

}//================================================================================


